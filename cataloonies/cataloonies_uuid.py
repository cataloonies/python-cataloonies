"""
cataloonies_uuid --- tools for issuing grid UUIDs for many kinds of grids
"""

import uuid
import hashlib
import functools

import numpy as np
import xarray as xr

NAMESPACE_ROOT = uuid.UUID('9cb1c6ed-9456-4dd9-87a4-33c410181828')  # a randomly chosen UUID4
NAMESPACE_GAUSSIAN_REDUCED = uuid.uuid5(NAMESPACE_ROOT, "gaussian_reduced")
NAMESPACE_FESOM_MESH = uuid.uuid5(NAMESPACE_ROOT, "FESOM-mesh")

def array_hashable_bytes(a):
    a = np.ascontiguousarray(a)
    return b"".join([
        a.dtype.str.encode("ascii"),
        bytes(np.int64(len(a))),
        bytes(a)
    ])

def detect_gaussian_reduced_grids(ds: xr.Dataset):
    if "lat" in ds and "reduced_points" in ds and \
            ds["lat"].shape == ds["reduced_points"].shape and len(ds["lat"].shape) == 1:
        
        lat = ds["lat"].values
        reduced_points = ds["reduced_points"].values
        
        h = hashlib.sha1(b"".join(map(array_hashable_bytes, [lat, reduced_points])))
        uuid_of_h_grid = uuid.uuid5(NAMESPACE_GAUSSIAN_REDUCED, h.hexdigest())
        
        yield {
            "type": "gaussian_reduced",
            "kind": "horizontal",
            "uuid": uuid_of_h_grid,
        }

def detect_uuid_grids(ds: xr.Dataset):
    if "uuidOfHGrid" in ds.attrs:
        yield {
            "kind": "horizontal",
            "uuid": uuid.UUID(ds.attrs["uuidOfHGrid"]),
        }
    if "uuidOfVGrid" in ds.attrs:
        yield {
            "kind": "vertical",
            "uuid": uuid.UUID(ds.attrs["uuidOfVGrid"]),
        }

@functools.lru_cache(maxsize=1)
def get_fesom_mesh_inverse_lookup():
    import requests
    import yaml
    meshtable = yaml.load(requests.get("https://github.com/FESOM/fesom2/raw/master/setups/paths.yml").content, Loader=yaml.SafeLoader)
    return {
        path: name
        for env, config in meshtable.items()
        for name, path in config.get("meshes", {}).items()
    }

def detect_fesom_grids(ds: xr.Dataset):
    if "grid_file_path" in ds.attrs:
        grid_file_path = ds.attrs["grid_file_path"]
        for path, name in get_fesom_mesh_inverse_lookup().items():
            if grid_file_path.startswith(path):
                yield {
                    "fesom_mesh_name": name,
                    "kind": "horizontal",
                    "uuid": uuid.uuid5(NAMESPACE_FESOM_MESH, name),
                }

grid_detectors = [
    detect_gaussian_reduced_grids,
    detect_uuid_grids,
    detect_fesom_grids,
]

def detect_grids(ds: xr.Dataset):
    grids = []
    for detector in grid_detectors:
        grids += list(detector(ds))
    return grids