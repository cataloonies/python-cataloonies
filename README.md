# cataloonies python package

This package contains helpers for model output catalog generation.

Currently, there are only tools to generate unique identifiers for model grids.

## grid identification

General idea:

```python
import cataloonies

#ds = some model output dataset
print(cataloonies.detect_grids(ds))
```
should return something like

```
[{'kind': 'horizontal', 'uuid': UUID('0f1e7d66-637e-11e8-913b-51232bb4d8f9')}]
```
